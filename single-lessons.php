<?php get_header(); ?>
<section class="container">
	<div class="row">
		<?php if (have_posts()) : while (have_posts()) : the_post();?>
		<div class="col-md-8">
			<h1 <?php if(get_field('_custom_color') == 'true') echo 'style="color:'.get_field('select_color').'"'?>><?php echo (get_post_meta($post->ID, '_custom_title', true) ? get_post_meta($post->ID, '_custom_title', true) : $post->post_title); ?></h1>
			<?php if(get_field('_page_intro')) echo '<div class="page-intro">'.get_field('_page_intro', false, false).'</div>';?>
			<?php the_content(); ?>
			<?php remove_filter ('acf_the_content', 'wpautop');?>
			<?php the_field('video');?>
			<?php add_filter ('acf_the_content', 'wpautop');?>
			<?php if(get_field('quize_shortcode')):?>
			<?php echo do_shortcode(get_field('quize_shortcode'));?>
			<?php endif;?>
		</div>
		<?php endwhile; endif; ?>
		<div class="col-md-3 col-md-offset-1">
			<div id="sidebar" class="blog">
				<?php dynamic_sidebar('lesson-sidebar'); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>