<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Loading any library such as jQuery that need to be managed from wp_header
 * Reply comments on single posts.
 */

add_action('wp_enqueue_scripts', 'berc_enqueue_assets');
function berc_enqueue_assets() {
	$child_theme_root =  str_replace("style.css", "", get_stylesheet_uri());
	
	wp_enqueue_style( 'berc_styles',  $child_theme_root. '/assets/css/styles.css' );
}

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
add_filter( 'body_class', 'berc_body_classes' );
function berc_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_front_page() || is_home() ) {
		$classes[] = 'berc';
	}
	return $classes;
}

function berc_field( $before = '', $after = '', $name, $sub = false ) {
	if ( true == $sub ) {
		$output = get_sub_field( $name );
	} else {
		$output = get_field( $name );
	}

	if ( empty( $output ) ) {
		return;
	}

	echo $before . $output . $after;
}
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'resource-post-thumbnail', 360, 360, true );
	add_image_size( 'lessons-post-thumbnail', 310, 271, true );
}

/**
 * Numeric pagination
 */
if ( !function_exists( 'cp_numeric_posts_nav' ) ) {

	function cp_numeric_posts_nav( $navigation_id = '' ) {

		if ( is_singular() )
			return;

		global $wp_query, $paged;
		/** Stop execution if there's only 1 page */
		if ( $wp_query->max_num_pages <= 1 )
			return;

		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

		$max = intval( $wp_query->max_num_pages );

		/** 	Add current page to the array */
		if ( $paged >= 1 )
			$links[] = $paged;

		/** 	Add the pages around the current page to the array */
		if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}

		if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}

		if ( $navigation_id != '' ) {
			$id = 'id="' . $navigation_id . '"';
		} else {
			$id = '';
		}

		echo '<div class="navigation" ' . $id . '><ul>' . "\n";

		/** 	Previous Post Link */
		if ( get_previous_posts_link() )
			printf( '<li>%s</li>' . "\n", get_previous_posts_link( '<span class="meta-nav">&larr;</span>' ) );

		/** 	Link to first page, plus ellipses if necessary */
		if ( !in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="active"' : '';

			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

			if ( !in_array( 2, $links ) )
				echo '<li>…</li>';
		}

		/** 	Link to current page, plus 2 pages in either direction if necessary */
		sort( $links );
		foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
		}

		/** 	Link to last page, plus ellipses if necessary */
		if ( !in_array( $max, $links ) ) {
			if ( !in_array( $max - 1, $links ) )
				echo '<li>…</li>' . "\n";

			$class = $paged == $max ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
		}

		/** 	Next Post Link */
		if ( get_next_posts_link() )
			printf( '<li>%s</li>' . "\n", get_next_posts_link( '<span class="meta-nav">&rarr;</span>' ) );

		echo '</ul></div>' . "\n";
	}

}
// Lessons Post Type registration
add_action('init', 'lessons_custom_init');
function lessons_custom_init() 
{
  	$labels = array(
	    'name' => _x('Lessons', 'post type general name'),
	    'singular_name' => _x('Lesson', 'post type singular name'),
	    'add_new' => _x('Add New', 'Lesson'),
	    'add_new_item' => __('Add New Lesson'),
	    'edit_item' => __('Edit Lesson'),
	    'new_item' => __('New Lesson'),
	    'view_item' => __('View Lesson'),
	    'search_items' => __('Search Lessons'),
	    'not_found' =>  __('No Lessons found'),
	    'not_found_in_trash' => __('No Lessons found in Trash'), 
	    'parent_item_colon' => '',
	    'menu_name' => 'Lessons'
  	);
  
  	$args = array(
    	'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'show_in_menu' => true, 
	    'query_var' => true,
	    'rewrite' => true,
	    'capability_type' => 'post',
	    'has_archive' => false, 
	    'hierarchical' => false,
	    'menu_position' => null,
	    'supports' => array('title','editor', 'excerpt', 'thumbnail'),
	); 
  	register_post_type('lessons',$args);
flush_rewrite_rules( false );
}
/**
 * Deregister custom post types.
 */
function custom_unregister_theme_post_types() {
    global $wp_post_types;
    foreach( array( 'aof', 'people', 'partners' ) as $post_type ) {
        if ( isset( $wp_post_types[ $post_type ] ) ) {
            unset( $wp_post_types[ $post_type ] );
        }
    }
}
add_action( 'init', 'custom_unregister_theme_post_types', 20 );