<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php wp_title('|',true,'right'); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="robots" content="noodp">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" rel="shortcut icon">
	<?php wp_head(); ?>
	
	<!--berc dev-->
	
</head>
<body <?php body_class(); ?>>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71973753-1', 'auto');
  ga('send', 'pageview');

</script>		
	
<?php
	global $curr_page;
	$curr_page = $wp_query->get_queried_object();
	$lang_fr = "";
	$lang_en = "active";
	if( is_page_template('pt-french.php') ){
		$lang_fr = "active";
		$lang_en = "";
	}
?>
<div class="viewport" id="top">
<section class="global-nav berc">
	<nav class="navbar navbar-inverse hidden-xs">
		<div class="container">
			<div class="">
				<ul class="nav navbar-nav navbar-left hidden-xs">
			  		<li><a href="http://www.kairoscanada.org" style="padding-left: 0;">A Resource of <span style="text-decoration:underline;">Kairos Canada</span></a>
			  		<!--<a href="<?php echo network_site_url(); ?>" style="padding-left: 0;">A Resource of <span style="text-decoration:underline;">Kairos Canada</span></a>--></li>
		  		</ul>
				<ul class="nav navbar-nav navbar-right">
		  			<li><a href="https://www.flickr.com/photos/kairoscanada"><span class="icon-flickr"></span></a></li>
		  			<li><a href="https://www.youtube.com/user/KAIROSCanada"><span class="icon-youtube"></span></a></li>
		  			<li><a href="https://twitter.com/kairoscanada"><span class="icon-twitter"></span></a></li>
		  			<li><a href="https://www.facebook.com/pages/Kairos-Canadian-Ecumenical-Justice-Initiatives/19277141685"><span class="icon-facebook"></span></a></li>
		  		</ul>
			</div>
		</div>
	</nav>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand visible-xs" href="<?php echo home_url();?>">
					The Blanket Exercise
				</a>
				<a class="navbar-brand hidden-xs" href="<?php echo home_url();?>">
					<img src="<?php echo get_template_directory_uri();?>/assets/images/logo.01.png" alt="<?php bloginfo('name'); ?>" />
	  			</a>
	  		</div>
	  		<div class="collapse navbar-collapse" id="navigation">
		  		<?php /* Primary navigation */
				wp_nav_menu( array(
				  'menu' => 'Main Menu',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav navbar-right',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				?>
				<div class="clearfix hidden-xs"></div>
				<?php /* Primary navigation */
				wp_nav_menu( array(
				  'menu' => 'Secondary Main Menu',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav navbar-right jk-tweak',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
				?>
	  		</div>
		</div>
	</nav>
	<div class="decor hidden-xs">
		<div class="bg hidden-xs"></div>
	</div>
</section>

<main id="main">
