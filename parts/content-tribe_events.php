	<?php if ( has_post_thumbnail() ): ?>
		<a href="<?php echo tribe_get_event_link(); ?>"><?php the_post_thumbnail('homepage-thumb',array( 'class'	=> "img-responsive"));?></a>
	<?php endif; ?>
	<?php $sep = ""; if ( tribe_get_city() && tribe_get_stateprovince() ) $sep = ", "; ?>
	<h2><a href="<?php echo tribe_get_event_link(); ?>"><?php the_title(); ?></a></h2>
	<?php echo tribe_events_event_schedule_details( $event_id, '<span class="date small">', '</span>' ); ?>
	<h3><?php echo tribe_get_city(). $sep . tribe_get_stateprovince();?></h3>
	<?php the_excerpt(); ?>
