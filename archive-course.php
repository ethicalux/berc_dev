<?php get_header(); ?>
<section class="container">
	<div class="row">
		<div class="col-md-8">
			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h1 class="page-title">Courses</h1>
					<?php
					// Show an optional term description.
					$term_description = term_description();

					if ( !empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
					?>
				</header><!-- .page-header -->

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
					// $status = the_post();
					if ( 'publish' != get_post_status() ) {
						continue;
					}
					get_template_part( 'parts/content-course' );
					?>

				<?php endwhile; ?>

				<?php cp_numeric_posts_nav( 'navigation-pagination' ); ?>

			<?php else : ?>

				<h1>Not Found</h1>

			<?php endif; ?>
		</div>
		<?php get_template_part( 'parts/sidebar'); ?>
	</div>
</section>
<?php get_footer(); ?>
